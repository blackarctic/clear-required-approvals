export type InjectedEnv = {
  RENOVATE_CONFIG_FILE?: string;
  RENOVATE_TOKEN?: string;
  CLEAR_REQUIRED_APPROVALS_LABEL?: string;
};

export type Env = {
  RENOVATE_TOKEN: string;
  ENDPOINT: string;
  DRY_RUN: boolean;
  PLATFORM: string;
  REPOSITORIES: string[];
  CLEAR_REQUIRED_APPROVALS_LABEL: string;
};
